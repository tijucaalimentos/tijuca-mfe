/**
 * @package Tijuca Alimentos MFE
 * @name TijucaMfe
 * @version 1.0.0
 * @description Programa de integração do PDV com MFe usando Electron
 * @copyright (c) 2012-2017, Tijuca Alimentos LTDA
 * 
 * @author Eduardo Almeida
*/

const electron = require('electron')

const {Menu, Tray} = require('electron')

const app = electron.app

const BrowserWindow = electron.BrowserWindow

const path = require('path')

const url = require('url')

let   mainWindow

if (require('electron-squirrel-startup')) return;
const handleStartupEvent = function() {
  if (process.platform !== 'win32') {
    return false;
  }
  const squirrelCommand = process.argv[1];
  switch (squirrelCommand) {
    case '--squirrel-install':
    case '--squirrel-updated':
      app.quit();
      return true;
    case '--squirrel-uninstall':
      app.quit();
      return true;
    case '--squirrel-obsolete':
      app.quit();
      return true;
  }
};
 
if (handleStartupEvent()) {
  return;
}

/**
 * @function showWindow
 * @description Mostra a janela
 */
const showWindow = () => {
    mainWindow = new BrowserWindow({width: 1000, height: 800})

    mainWindow.loadURL(url.format({
      pathname: path.join(__dirname, 'app/index.html'),
      protocol: 'file:',
      slashes: true
    })) 

    mainWindow.on('closed',  () => {
    })

    // mainWindow.setMenu(null)
}

/**
 * @function openTijucaMFE
 * @description Inicia o programa
 */
const openTijucaMFE = () => {
  
  /**
   * @function WebServer
   * @description Ao iniciar o programa, levantar o WebServer
   */
  const {exp} = require('./app/bin');
  exp.listen(3000,() => {
    console.log('server on');
  })
  

  /**
   * @function TrayIcon
   * @description Colocar icone na barra de tarefas do sistema
   */
  appIcon = new Tray(path.join(__dirname, 'app/icon.png'))
  const contextMenu = Menu.buildFromTemplate([
    {label: 'Exibir', click: res => showWindow()},
    {label: 'Sair', click: res => app.quit()},
  ])
  appIcon.setContextMenu(contextMenu)

}

/**
 * @function closeWindow
 * @description Fecha o programa
 */
const closeWindow = () => {}

/**
 * @function activate
 * @description Ativa o programa
 */
const activate = () => {
  if (mainWindow === null) {
    openTijucaMFE()
  }
}

app.on('ready', openTijucaMFE)
app.on('window-all-closed', closeWindow)
app.on('activate', activate)