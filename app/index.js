// Importando as bibliotecas
global.jQuery   = window.jQuery = window.$ = $ = require("jquery");
global.Tether   = window.Tether = require('tether');
window.Popper = require('popper.js').default;

// Chamando de estilo principal
const bootstrap     = require("bootstrap");
const dataTables    = require("datatables");


(function(){
    $(".table").dataTable();
})()