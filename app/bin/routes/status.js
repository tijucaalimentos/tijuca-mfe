/**
 * @module StatusMFE 
 * @description Route ExpressJS WebServer
 * @param {req, res} RotaStatusWebServer /status
*/

module.exports = (req, res) => {

    res.header("Access-Control-Allow-Origin", "*");

    res.header("x-powered-by", "Tijuca Alimentos");

    let response = {
        name: "Tijuca MFe - WebService",
        version: "1.0.0",   
        status: "online"
    }


    res.json(response);   
}