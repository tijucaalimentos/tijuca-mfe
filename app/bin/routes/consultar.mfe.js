/**
 * @module ConsultarMFE 
 * @description Route ExpressJS WebServer
 * @param {req, res} ConsultarMFE /check/mfe
*/

const Integrador = require("../integrador");

module.exports = (req, res) => {
    
    res.header("Access-Control-Allow-Origin", "*");
    
    res.header("x-powered-by", "Tijuca Alimentos");

    let data = req.body;

    Integrador.ConsultarMFE(Integrador.numeroAleatorio(),Integrador.numeroAleatorio())
    .then((response) => {
        res.end(response);
    })
    .catch((error) => {
        res.end(error);
    })
    
}