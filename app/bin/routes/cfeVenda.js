/**
 * @module cfeVenda 
 * @description Route ExpressJS WebServer
 * @param {req, res} cfeVenda /cfe/venda
*/

const Integrador = require("../integrador");

module.exports = (req, res) => {
    
    res.header("Access-Control-Allow-Origin", "*");
    
    res.header("x-powered-by", "Tijuca Alimentos");

    // Recuperando XML gerado do PDV
    let xml = req.body.xml;

    Integrador.CfeVenda(Integrador.numeroAleatorio(), Integrador.numeroAleatorio(), xml)
    .then((response) => {
        res.end(response);
    })
    .catch((error) => {
        res.end(error);
    })
    
}