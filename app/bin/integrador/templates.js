/**
 * @function ConsultarMFE 
 * @description Usado para retornar o template da ConsultaMFE
 * @returns {String} Template ConsultarMFE
 * @param {Number, Number} Required identificador e numeroSessao podem ser numeros aleatorios 
*/
exports.ConsultarMFE = (identificador, numeroSessao) => {
return `<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Integrador>
<Identificador>
<Valor>${identificador}</Valor>
</Identificador>
<Componente Nome="MF-e">
<Metodo Nome="ConsultarMFe">
<Parametros>
<Parametro>
<Nome>numeroSessao</Nome>
<Valor>${numeroSessao}</Valor>
</Parametro>
</Parametros>
</Metodo>
</Componente>
</Integrador>`
}

/**
 * @function CFEVenda 
 * @description Usado para retornar o template da CFEVenda
 * @returns {String} Template CFEVenda
 * @param {Number, Number, codigoAtivacao, xml, nrDocumento} Required 
*/
exports.CFEVenda = (identificador, numeroSesssao, codigoAtivacao, xml) => {
return `<?xml version="1.0" encoding="utf-8" ?>
<Integrador>
<Identificador>
<Valor>${identificador}</Valor>
</Identificador>
<Componente Nome="MF-e">
<Metodo Nome="TesteFimAFim">
<Parametros>
<Parametro>
<Nome>numeroSessao</Nome>
<Valor>${numeroSesssao}</Valor>
</Parametro>
<Parametro>
<Nome>codigoDeAtivacao</Nome>
<Valor>${codigoAtivacao}</Valor>
</Parametro>
<Parametro>
<Nome>dadosVenda</Nome>
<Valor><![CDATA[${xml}]]></Valor>
</Parametro>
</Parametros>
</Metodo>
</Componente>
</Integrador>`
}