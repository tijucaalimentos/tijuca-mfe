/**
 * @module ConsultarMFE 
 * @description Executar funcao de ConsultaMFE 
 * @return { response } Resolve ou Reject
*/
const   Templates   = require("./templates"),
        fs          = require("fs"),
        index       = require("./index"),
        {parseString} = require('xml2js');
        
        
module.exports = (...args) => {
    return new Promise((resolve, reject) => {
        
        // Template
        let _tpl = Templates.ConsultarMFE(args[0],args[1]);

        let dados = {
            identificador: args[0],
            numeroSessao: args[1]
        }
    
        // Name
        let _filename = index.dataAtual();
        
        console.log(_filename,dados)

        fs.writeFile(`${index.paths().Input}/${_filename}.xml`, _tpl, ( error ) => {
            
            if (error) throw reject(error);

            fs.watch(`${index.paths().Output}`, {}, (eventType, filename) => {
                fs.readFile(`${index.paths().Output}/${filename}`,'utf8',(error, data) => {
                    parseString(data,(err, result) => {
                        if(result){
                            if(result.Integrador.Identificador[0].Valor[0] && result.Integrador.Identificador[0].Valor[0] == dados.identificador){
                                resolve(data);
                            }
                        }
                    })
                })
            });
        })
       
    })
}
