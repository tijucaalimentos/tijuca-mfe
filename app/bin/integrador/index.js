const OS = require("os");

/**
 * @description Require Module ConsultarMFE 
 */
exports.ConsultarMFE    = require('./consultar.mfe');

/**
 * @description Require Module of Templates 
 */
exports.templates       = require('./templates');

/**
 * @description Require Module CFEVenda 
 */
exports.CfeVenda        = require('./cfeVenda');

/**
 * @description Paths do Integrador - Pastas: Input e Output
 */
exports.paths = () => {
    if(OS.platform() == 'win32'){
        return {
            Input: "C:/Integrador/Input",
            Output: "C:/Integrador/Output",
        }
    }else{
        return {
            Input: "/home/eduardo/integrador/input",
            Output: "/home/eduardo/integrador/output",
        }
    }
}

/**
 * @function numeroAleatorio
 * @description Gerar numero de sessao aleatorio e salva-la
 */
exports.numeroAleatorio = () => {
    let rand = "" + Math.random()
    let name = rand.substring(1,7).replace(/\./g,"")
    return name;
}

/**
 * @function codigoAtivacao
 * @description Retornar o codigo de ativacao do MFE
 */
exports.codigoAtivacao = () => {
    return "tijucaMFE";
}

/**
 * @function dataAtual
 * @description retornar a data atual
 */
exports.dataAtual = () => {
    let d = new Date();
    let curr_date = d.getDate();
    let curr_month = d.getMonth() + 1; //Months are zero based
    let curr_year = d.getFullYear();
    let curr_hour = d.getHours();
    let curr_minute = d.getMinutes();
    let curr_seconds = d.getSeconds();
    
    if(curr_month < 10){
        curr_month = "0" + curr_month
    }else{
        curr_month = curr_month;
    }

    if(curr_date < 10){
        curr_date = "0" + curr_date
    }else{
        curr_date = curr_date;
    }
    
    if(curr_hour < 10){
        curr_hour = "0" + curr_hour
    }else{
        curr_hour = curr_hour;
    }

    if(curr_seconds < 10){
        curr_seconds = "0" + curr_seconds
    }else{
        curr_seconds = curr_seconds;
    }
    let name = curr_year + "" + curr_month + "" + curr_date + "" + curr_hour + "" + curr_minute + "" + curr_seconds;
    return name;
}