/**
 * @package Tijuca Alimentos MFE
 * @name TijucaMfe
 * @version 1.0.0
 * @description ExpressJS WebServer
 * @copyright (c) 2012-2017, Tijuca Alimentos LTDA
 * 
 * @author Eduardo Almeida
*/

const   express         = require('express'),
        bodyParser      = require('body-parser'),
        // Routes
        status          = require('./routes/status'),
        consultarMfe    = require('./routes/consultar.mfe'),
        cfeVenda        = require("./routes/cfeVenda"),
        // Main Functions 
        Integrador      = require("./integrador");

const   exp             = express();

// Enable POST JSON
exp.use(bodyParser.json());
exp.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


/**
 * Rotas ExpressJS
 */

// /
exp.get('/', (req,res) => status(req,res));

// Checar status do WebService 
exp.get('/status', (req,res) => status(req,res));

// Consultar status do equipamento
exp.post('/check/mfe', (req,res) => consultarMfe(req,res));
exp.use('/check/mfe', (req,res) => {
    // error
})

// CFe Venda
exp.use("/cfe/venda", (req,res) => cfeVenda(req,res));

exports.exp = exp;