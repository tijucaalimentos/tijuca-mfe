require("style-loader");
require("css-loader");
require("sass-loader");
const ExtractTextPlugin = require("extract-text-webpack-plugin")

module.exports = {
    entry: ["./app/index.js", "./app/styles/index.scss"],
    output: {
        filename: "app/dist/bundle.js"
    },
    module: {
        rules: [{ // regular css files
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
              loader: 'css-loader?importLoaders=1',
            }),
          },
          { // sass / scss loader for webpack
            test: /\.(sass|scss)$/,
            use: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
          }]
    },
    plugins: [
       new ExtractTextPlugin( {
          filename: "app/dist/bundle.css"
       }),
    ]
    
};